<?php
/**
 * Single Product Thumbnails
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product, $woocommerce;

$attachment_ids = $product->get_gallery_attachment_ids();

if ( $attachment_ids ) {
	?>

	<div class="single-product-slider-thumbs">
		<?php
		if ( has_post_thumbnail() ) {
			$image = get_the_post_thumbnail( $post->ID, apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail') );
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ) );
		} else {
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ) );
		}
		foreach ( $attachment_ids as $attachment_id ) {
			$image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_small_thumbnail_size', 'shop_thumbnail' ) );
			echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', sprintf( '%s', $image ) );
		}
		?>
	</div>

	<?php
}
