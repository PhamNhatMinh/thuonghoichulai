<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product; $eltd_moose_options;

if ( ! $product->is_purchasable() ) {
	return;
}

?>

<?php
	// Availability
	$availability = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>
	
	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>
        <div class="cart_inner_holder">
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

	 	<?php
			if ( ! $product->is_sold_individually() ) {
				woocommerce_quantity_input(array(
					'min_value' => apply_filters('woocommerce_quantity_input_min', 1, $product),
					'max_value' => apply_filters('woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product),
					'input_value' => ( isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : 1 )
				));
			}
	 	?>
		<?php 
		global $eltd_moose_options; $eltdIconCollections ;
		$button_classes = '';
			if(isset($eltd_moose_options['button_hover_animation'])){
				if($eltd_moose_options['button_hover_animation'] !== ''){
					$button_classes .= " {$eltd_moose_options['button_hover_animation']}";
					$button_classes .= " animate_button";
				}
			}
		?>	
	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
		<button type="submit" class="single_add_to_cart_button transparent qbutton button alt <?php echo esc_attr($button_classes)?> qbutton_with_icon icon_right">
			<span class="text_holder">
				<span class="a_overlay" ></span>
				<span><?php echo esc_html($product->single_add_to_cart_text()); ?></span>
				<span  class="hidden_text"><?php echo esc_html($product->single_add_to_cart_text()); ?></span>
			</span>
			<span class="icon_holder">
				<span><i class="eltd_icon_simple_line_icon icon-basket button_icon" style="width: inherit; "></i></span>
				<span class="hidden_icon"><i class="eltd_icon_simple_line_icon icon-basket button_icon" style="width: inherit; "></i></span>					
			</span>
		</button>
		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
        </div>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>