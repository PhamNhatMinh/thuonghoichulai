<?php
/**
 * Single Product title
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $eltd_moose_options;

?>
<h2 itemprop="name" class="product_title entry-title"><?php the_title(); ?></h2>

