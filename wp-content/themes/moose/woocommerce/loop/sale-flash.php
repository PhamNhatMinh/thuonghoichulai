<?php
/**
 * Product loop sale flash
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php if ($product->is_on_sale()) : ?>

	<?php echo apply_filters('woocommerce_sale_flash', '<span class="onsale onsale-outter"><span class="onsale-inner">'.__( 'Sale!', 'woocommerce' ).'</span></span>', $post, $product); ?>

<?php endif; ?>

<?php if ( ! $product->is_in_stock() ) : ?>

	<?php echo apply_filters('woocommerce_sale_flash', '<span class="out-of-stock out-of-stock-outter"><span class="out-of-stock-inner">'.__( 'Out of Stock', 'woocommerce' ).'</span></span>'); ?>

<?php endif; ?>
